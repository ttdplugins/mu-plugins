<?php 
/* 
Plugin Name: Automatic Updates Shiv.
Description: Enables automatic WP updates even though the site is using (VCS) Version Control Software.
Version: 1.0 
Author: Two Thirds Design Ltd
Author URI: http://www.twothirdsdesign.com 
License: GPLv2 
*/
// Allows Background Updates
add_filter( 'automatic_updates_is_vcs_checkout', '__return_false' );