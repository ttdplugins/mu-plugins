<?php
/* 
Plugin Name: Includes WP Default Dirs
Description: Includes WP Default Themes Directory
Version: 1.0 
Author: Two Thirds Design Ltd
Author URI: http://www.twothirdsdesign.com 
License: GPLv2 
*/

register_theme_directory( ABSPATH . 'wp-content/themes/' );